-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 10 Février 2015 à 16:29
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `japonais`
--

-- --------------------------------------------------------

--
-- Structure de la table `caractere`
--

CREATE TABLE IF NOT EXISTS `caractere` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ecriture` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `romaji` text NOT NULL,
  `alphabet` text NOT NULL,
  `section` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=110 ;

--
-- Contenu de la table `caractere`
--

INSERT INTO `caractere` (`id`, `ecriture`, `romaji`, `alphabet`, `section`) VALUES
(1, 'あ', 'a', 'hiragana', 1),
(2, 'い', 'i', 'hiragana', 1),
(3, 'う', 'u', 'hiragana', 1),
(4, 'え', 'e', 'hiragana', 1),
(5, 'お', 'o', 'hiragana', 1),
(6, 'か', 'ka', 'hiragana', 1),
(7, 'き', 'ki', 'hiragana', 1),
(8, 'く', 'ku', 'hiragana', 1),
(9, 'け', 'ke', 'hiragana', 1),
(10, 'こ', 'ko', 'hiragana', 1),
(11, 'さ', 'sa', 'hiragana', 1),
(12, 'し', 'shi', 'hiragana', 1),
(13, 'す', 'su', 'hiragana', 1),
(14, 'せ', 'se', 'hiragana', 1),
(15, 'そ', 'so', 'hiragana', 1),
(16, 'た', 'ta', 'hiragana', 1),
(17, 'ち', 'chi', 'hiragana', 1),
(18, 'つ', 'tsu', 'hiragana', 1),
(19, 'て', 'te', 'hiragana', 1),
(20, 'と', 'to', 'hiragana', 1),
(21, 'な', 'na', 'hiragana', 1),
(22, 'に', 'ni', 'hiragana', 1),
(23, 'ぬ', 'nu', 'hiragana', 1),
(24, 'ね', 'ne', 'hiragana', 1),
(25, 'の', 'no', 'hiragana', 1),
(26, 'は', 'ha', 'hiragana', 1),
(27, 'ひ', 'hi', 'hiragana', 1),
(28, 'ふ', 'fu', 'hiragana', 1),
(29, 'へ', 'he', 'hiragana', 1),
(30, 'ほ', 'ho', 'hiragana', 1),
(31, 'ま', 'ma', 'hiragana', 1),
(32, 'み', 'mi', 'hiragana', 1),
(33, 'む', 'mu', 'hiragana', 1),
(34, 'め', 'me', 'hiragana', 1),
(35, 'も', 'mo', 'hiragana', 1),
(36, 'や', 'ya', 'hiragana', 1),
(37, 'x', 'yi', 'hiragana', 1),
(38, 'ゆ', 'yu', 'hiragana', 1),
(39, 'x', 'ye', 'hiragana', 1),
(40, 'よ', 'yo', 'hiragana', 1),
(41, 'ら', 'ra', 'hiragana', 1),
(42, 'り', 'ri', 'hiragana', 1),
(43, 'る', 'ru', 'hiragana', 1),
(44, 'れ', 're', 'hiragana', 1),
(45, 'ろ', 'ro', 'hiragana', 1),
(46, 'わ', 'wa', 'hiragana', 1),
(47, 'ゐ', 'wi', 'hiragana', 1),
(48, 'x', 'wu', 'hiragana', 1),
(49, 'ゑ', 'we', 'hiragana', 1),
(50, 'を', 'wo', 'hiragana', 1),
(51, 'ん', 'n', 'hiragana', 1),
(52, 'が', 'ga', 'hiragana', 2),
(53, 'ぎ', 'gi', 'hiragana', 2),
(54, 'ぐ', 'gu', 'hiragana', 2),
(55, 'げ', 'ge', 'hiragana', 2),
(56, 'ご', 'go', 'hiragana', 2),
(57, 'ざ', 'za', 'hiragana', 2),
(58, 'じ', 'ji', 'hiragana', 2),
(59, 'ず', 'zu', 'hiragana', 2),
(60, 'ぜ', 'ze', 'hiragana', 2),
(61, 'ぞ', 'zo', 'hiragana', 2),
(62, 'だ', 'da', 'hiragana', 2),
(63, 'ぢ', 'dji', 'hiragana', 2),
(64, 'づ', 'du', 'hiragana', 2),
(65, 'で', 'de', 'hiragana', 2),
(66, 'ど', 'do', 'hiragana', 2),
(67, 'ば', 'ba', 'hiragana', 2),
(68, 'び', 'bi', 'hiragana', 2),
(69, 'ぶ', 'bu', 'hiragana', 2),
(70, 'べ', 'be', 'hiragana', 2),
(71, 'ぼ', 'bo', 'hiragana', 2),
(72, 'ぱ', 'pa', 'hiragana', 2),
(73, 'ぴ', 'pi', 'hiragana', 2),
(74, 'ぷ', 'pu', 'hiragana', 2),
(75, 'ぺ', 'pe', 'hiragana', 2),
(76, 'ぽ', 'po', 'hiragana', 2),
(77, 'きゃ', 'kya', 'hiragana', 3),
(78, 'きゅ', 'kyu', 'hiragana', 3),
(79, 'きょ', 'kyo', 'hiragana', 3),
(80, 'ぎゃ', 'gya', 'hiragana', 3),
(81, 'ぎゅ', 'gyu', 'hiragana', 3),
(82, 'ぎょ ', 'gyo', 'hiragana', 3),
(83, 'しゃ', 'sha', 'hiragana', 3),
(84, 'しゅ', 'shu', 'hiragana', 3),
(85, 'しょ', 'sho', 'hiragana', 3),
(86, 'じゃ', 'ja', 'hiragana', 3),
(87, 'じゅ', 'ju', 'hiragana', 3),
(88, 'じょ', 'jo', 'hiragana', 3),
(89, 'ちゃ ', 'cha', 'hiragana', 3),
(90, 'ちゅ', 'chu', 'hiragana', 3),
(91, 'ちょ', 'cho', 'hiragana', 3),
(92, 'にゃ', 'nya', 'hiragana', 3),
(93, 'にゅ', 'nyu', 'hiragana', 3),
(94, 'にょ', 'nyo', 'hiragana', 3),
(95, 'ひゃ', 'hya', 'hiragana', 3),
(96, 'ひゅ', 'hyu', 'hiragana', 3),
(97, 'ひょ', 'hyo', 'hiragana', 3),
(98, 'びゃ', 'bya', 'hiragana', 3),
(99, 'びゅ', 'byu', 'hiragana', 3),
(100, 'びょ', 'byo', 'hiragana', 3),
(101, 'ぴゃ', 'pya', 'hiragana', 3),
(102, 'ぴゅ', 'pyu', 'hiragana', 3),
(103, 'ぴょ', 'pyo', 'hiragana', 3),
(104, 'みゃ', 'mya', 'hiragana', 3),
(105, 'みゅ', 'myu', 'hiragana', 3),
(106, 'みょ', 'myo', 'hiragana', 3),
(107, 'りゃ', 'rya', 'hiragana', 3),
(108, 'りゅ', 'ryu', 'hiragana', 3),
(109, 'りょ', 'ryo', 'hiragana', 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
